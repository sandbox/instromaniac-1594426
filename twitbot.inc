<?php
/*
 * @file Library of functions for supporting TwitBot functionality.
 */

/**
 * Get an array of new mentions on Twitter for the given twitter uid.
 * 
 * @param int $twitter_uid 
 *   The twitter uid to get mentions for.
 * @param int $since_twitter_id
 *   The id of the last twitter post that was fetched for this $twitter_uid.
 *   defaults to 0.
 * @param bool $persist
 *   When TRUE the posts will be saved so they can be processed at a later time.
 *   Defaults to TRUE.
 * @return array
 *   An array of twitter posts. See the Twitter REST API docs for a complete 
 *   list of properties.
 */
function twitbot_mentions_fetch($twitter_uid, $since_twitter_id = 0, $persist = TRUE) {
  module_load_include('inc', 'twitter', 'twitter');
  $account = twitter_account_load($twitter_uid);
  // Set up a connection with twitter using the given account.
  $twitter = twitter_connect($account);
  // Fetch mentions for the curret account since the last message id that was
  // previously fetched. If no posts were fetch we default to 0.
  // @see https://dev.twitter.com/docs/api/1/get/statuses/mentions for a list
  // of parameters.
  // @todo support more than 200 messages per run.
  $params = array(
    'count' => 200,
    'since_id' => $since_twitter_id, 
  );
  // Fetch the mentions for the given $twitter_uid that occured since the post
  // with ID $since_twitter_id.
  try {
    $mentions = $twitter->mentions($params);
    // Store the messages if requested.
    if ($persist) {
      twitbot_mentions_save($mentions, $twitter_uid);
    }
    return $mentions;
  }
  catch (Exception $e) {
    watchdog_exception('twitter', $e);
  }
  return array();
}

/**
 * Save mentions to the database.
 * 
 * @param array $mentions 
 *   An array of TwitterStatus objects as fetched from the Twitter REST API using
 *   twitter module.
 * @param int $target_uid
 *   The twitter uid of the account that was mentioned.
 */
function twitbot_mentions_save($mentions, $target_uid) {
  foreach ($mentions as $mention) {
    twitbot_mention_save($mention, $target_uid);
  }
}

/**
 * Save a mention to the database.
 * 
 * @param TwitterStatus $mention
 *   
 * @param int $target_uid 
 */
function twitbot_mention_save($mention, $target_uid) {
  module_load_include('inc', 'twitter', 'twitter');
  // Save the message in twitter module's table.
  twitter_status_save($mention);
  // Save meta data so the message can be processed at a later stage.
  $status = array(
    'twitter_id' => $mention->id,
    'target_twitter_uid' => $target_uid,
  );
  twitbot_twitter_status_meta_save($status);
}

/**
 * Process a newly imported twitter mention.
 * 
 * @param TwitterStatus $mention
 *   A TwitterStatus object containing all the mention data.
 * @param TwitterUser $twitter_account
 *   The Twitter account of the user that was mentioned.
 */
function twitbot_mention_process($mention, $twitter_account) {
  // Set the mention's meta data.
  $time = time();
  $status = array(
    'twitter_id' => $mention->id,
    'is_processed' => 1,
    'processed_at' => $time,
    'is_queued' => 0,
  );
  db_merge('twitbot_posts')
    ->key(array('twitter_id' => $status['twitter_id']))
    ->fields($status)
    ->execute();
  // Process a new mention so the bot can do something with it.
  module_invoke_all('twitbot_mention_process', $mention, $twitter_account);
}

/**
 * Load additional twitter account info needed for twitbot. Returns the fields
 * from the {twitbot_account} table.
 * 
 * @param int $twitter_uid 
 *   The twitter id of the account.
 * @return object
 *   An object of values from the {twitbot_account} table. Uses the static 
 *   cache.
 */
function twitbot_twitter_account_meta_load($twitter_uid) {
  $accounts = &drupal_static(__FUNCTION__, array());
  if (!isset($accounts[$twitter_uid])) {
    $query = db_select('twitbot_account', 'tba')
      ->fields('tba')
      ->condition('tba.twitter_uid', $twitter_uid);
    $accounts[$twitter_uid] = $query->execute()->fetch();
  }
  return $accounts[$twitter_uid];
}

/**
 * Save meta data for a twitter user account.
 * 
 * @param object $account 
 *   An object with the same properties as the {twitbot_account} table.
 */
function twitbot_twitter_account_meta_save($account) {
  twitbot_static_cache_reset();
  $fields = (array) $account;
  db_merge('twitbot_account')
    ->key(array('twitter_uid' => $fields['twitter_uid']))
    ->fields($fields)
    ->execute();
}

/**
 * Remove TwitBot meta data for delete accounts.
 * 
 * @param int $twitter_uid 
 *   The twitter uid of the account to delete meta data for.
 * 
 * @todo check if we should delete items from the queue.
 */
function twitbot_twitter_account_meta_delete($twitter_uid) {
  twitbot_static_cache_reset();
  // Delete account data.
  $query = db_delete('twitbot_account');
  $query->condition('twitter_uid', $twitter_uid);
  $query->execute();
  // Delete twitter mentions.
  $query = db_delete('twitbot_posts');
  $query->condition('target_twitter_uid', $twitter_uid);
  $query->execute();
}

/**
 * Load a drupal user account by its twitter ID.
 * @param type $twitter_uid 
 */
function twitbot_user_load_by_twitter_uid($twitter_uid) {
  $accounts = &drupal_static(__FUNCTION__, array());
  if (!isset($accounts[$twitter_uid])) {
    $accounts[$twitter_uid] = NULL;
    $query = db_select('twitter_account')
      ->fields('uid')
      ->condition('twitter_uid', $twitter_uid);
    if ($uid = $query->execute()->fetchCol()) {
      $accounts[$twitter_uid] = user_load($uid);
    }
  }
  return $accounts[$twitter_uid];
}

/**
 * Wrapper around twitter_account_load(). Next to adding twitbot meta data to
 * the TwitterUser object we use a static cache. This prevents loading identical
 * read-only objects. The TwitBot meta data is stored in a property called
 * twitbot.
 * 
 * @param type $twitter_uid
 *   The Twitter ID of the account.
 * 
 * @return TwitterUser
 *   A TwitterUser object with additional twitbot meta data. Uses the static
 *   cache.
 */
function twitbot_twitter_account_load($twitter_uid) {
  $accounts = &drupal_static(__FUNCTION__, array());
  if (!isset($accounts[$twitter_uid])) {
    module_load_include('inc', 'twitter', 'twitter');
    $accounts[$twitter_uid] = twitter_account_load($twitter_uid);
    $accounts[$twitter_uid]->twitbot = twitbot_twitter_account_meta_load($twitter_uid);
  }
  return $accounts[$twitter_uid];
}

/**
 * Load additional twitter status info needed for twitbot. Returns the fields
 * from the {twitbot_posts} table.
 * 
 * @param int $twitter_id 
 *   The twitter id of the status.
 * @return object
 *   An object of values from the {twitbot_posts} table. Uses the static 
 *   cache.
 */
function twitbot_twitter_status_meta_load($twitter_id) {
  $posts = &drupal_static(__FUNCTION__, array());
  if (!isset($posts[$twitter_id])) {
    $query = db_select('twitbot_posts', 'tbp')
      ->fields('tbp')
      ->condition('tbp.twitter_id', $twitter_id);
    $posts[$twitter_id] = $query->execute()->fetch();
  }
  return $posts[$twitter_id];
}

/**
 * Save meta data for a twitter status.
 * 
 * @param object $status 
 *   An object with the same properties as the {twitbot_posts} table.
 */
function twitbot_twitter_status_meta_save($status) {
  twitbot_static_cache_reset('twitter_status');
  $fields = (array) $status;
  db_merge('twitbot_posts')
    ->key(array('twitter_id' => $fields['twitter_id']))
    ->fields($fields)
    ->execute();
}

/**
 * Remove TwitBot meta data for deleted statuses.
 * 
 * @param int $twitter_id 
 *   The twitter ID of the status to delete meta data for.
 * 
 * @todo check if we should delete items from the queue.
 */
function twitbot_twitter_status_meta_delete($twitter_id) {
  twitbot_static_cache_reset('twitter_status');
  // Delete account data.
  $query = db_delete('twitbot_posts');
  $query->condition('twitter_id', $twitter_id);
  $query->execute();
}

/**
 * Load a TwitterStatus object from the database and add the additional TwitBot
 * meta data to the object as a twitbot property.
 * 
 * @param type $twitter_id
 *   The Twitter ID of the status.
 * 
 * @return TwitterStatus
 *   A TwitterStatus object with additional twitbot meta data. Uses the static
 *   cache.
 */
function twitbot_twitter_status_load($twitter_id) {
  $posts = &drupal_static(__FUNCTION__, array());
  if (!isset($posts[$twitter_id])) {
    module_load_include('inc', 'twitter', 'twitter');
    $query = db_select('twitter', 'tw')
      ->fields('tw')
      ->condition('tw.twitter_id', $twitter_id);
    $posts[$twitter_id] = $query->execute()->fetch();
    $posts[$twitter_id]->twitbot = twitbot_twitter_status_meta_load($twitter_id);
  }
  return $posts[$twitter_id];
}

/**
 * Resets the twitbot static caches. 
 */
function twitbot_static_cache_reset($type = 'twitter_account') {
  switch ($type) {
    case 'twitter_account':
      drupal_static_reset('twitbot_twitter_account_load');
      drupal_static_reset('twitbot_twitter_account_meta_load');
      break;
    case 'twitter_status':
      drupal_static_reset('twitbot_twitter_status_load');
      drupal_static_reset('twitbot_twitter_status_meta_load');
      break;
  }
}
