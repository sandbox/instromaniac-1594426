<?php

/**
 * @file twitbot_trigger.module
 * 
 * Provides integration with the trigger module. Provides the following data:
 * - Events:
 *     - twitbot_mentioned: Fired when an account is mentioned on twitter.
 */

/**
 * Implements hook_trigger_info().
 * 
 */
function twitbot_trigger_trigger_info() {
  return array(
    'twitbot' => array(
      'twitbot_mentioned' => array(
        'label' => t('A Twitter mention has been received.'),
      ),
    ),
  );
}

/**
 * Implements hook_twitbot_mention_process().
 *  
 */
function twitbot_trigger_twitbot_mention_process($mention, $twitter_account) {
  // Set up the target and acting user accounts.
  $target_user = user_load($twitter_account->uid);
  // Integrate with the trigger module.
  if ($target_user) {
    $context = array(
      'twitbot_mention' => $mention->text,
      'twitbot_target_account' => $twitter_account,
    );
    // Find and fire all actions assigned to the twitbot_mention_process trigger.
    foreach (array_keys(trigger_get_assigned_actions('twitbot_mention_process'))  as $aid) {
      // The 'if ($aid)' is a safeguard against http://drupal.org/node/271460#comment-886564
      if ($aid) {
        actions_do($aid, $target_user, $context);
      }
    }
  }
}
