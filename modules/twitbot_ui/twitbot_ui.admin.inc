<?php
/**
 * @file twitbot_ui.admin.inc 
 * 
 * Administrative forms and callbacks for configuring TwitBot.
 * 
 */

/**
 * Form that allows you to configure how TwitBot will fetch mentions. The
 * following settings are provided:
 * 
 * - Select the channel for each method.
 * - Set the amount of mentions and accounts TwitBot should handle per cron
 *   run.
 */
function twitbot_ui_admin_form($form, &$form_state) {
  module_load_include('inc', 'twitbot', 'plugins');
  $form = array();
  // Matrix for setting which channels a method will use to fetch its data.
  // Get the method and channel ctools plugins.
  $methods = twitbot_get_methods();
  $channels = twitbot_get_channels();
  // Fetch the previously stored settings.
  $matrix = variable_get('twitbot_method_channel_matrix');
  // Set up the channel options. Each method can choose from the same set of
  // channels so we only do this once.
  $channel_options = array();
  foreach ($channels as $channel) {
    $channel_options[$channel['name']] = $channel['title'];
  }
  // Set up the wrapper elemet for the methods. 
  $form['twitbot_method_channel_matrix'] = array(
    '#tree' => TRUE,
    '#type' => 'fieldset',
    '#title' => t('Channels'),
    '#description' => t('Select which channels each of the following methods 
      should use for fetching Twitter messages. If you don\'t choose a channel
      for a certain method the method won\'t be used.')
  );
  // Each method has its own set of channel settings.
  foreach ($methods as $method) {
    $form['twitbot_method_channel_matrix'][$method['name']] = array(
      '#title' => $method['title'],
      '#type' => 'checkboxes',
      '#options' => $channel_options,
      '#default_value' => $matrix[$method['name']],
    );
  }
  return system_settings_form($form);
}
