<?php

/**
 * @file twitbot_rules.rules.inc
 * Rules integration for the TwitBot module.
 */

/**
 * Implements hook_rules_event_info().
 * @todo support multiple target users.
 */
function twitbot_rules_rules_event_info() {
  $items = array();
  $variables = array(
    'mention' => array(
      'type' => 'text',
      'label' => t('Twitter mention text'),
    ),
    'target_screen_name' => array(
      'type' => 'text',
      'label' => t('Target Twitter username'),
    ),
    'author_screen_name' => array(
      'type' => 'text',
      'label' => t('Twitter mention author username'),
    ),
    'target_user' => array(
      'type' => 'user',
      'label' => t('Target user account'),
    ),
    'author_user' => array(
      'type' => 'user',
      'label' => t('Twitter mention author user account'),
    ),
  );

  $items['twitbot_mentioned'] = array(
    'group' => t('Twitter'),
    'label' => t('A Twitter user has been mentioned'),
    'variables' => $variables,
    'access callback' => 'twitbot_rules_integration_access',
  );
  return $items;
}

/**
 * Implements hook_rules_action_info().
 */
function twitbot_rules_rules_action_info() {
  $return = array();
  $return['twitbot_rules_action_user_fetch'] = array(
    'label' => t('Fetch user by Twitter username'),
    'parameter' => array(
      'twitter_uid' => array('type' => 'int', 'label' => t('Twitter user id')),
    ),
    'provides' => array(
      'user' => array('type' => 'user', 'label' => t('User')),
    ),
    'group' => t('Twitter'),
    'access callback' => 'twitbot_rules_integration_access',
  );
  
  return $return;
}

/**
 * Rules callback for twitbot events. Returns TRUE if the logged in user is
 * allowed to configure twitbot rules data. 
 */
function twitbot_rules_integration_access($type, $name) {
  return user_access('administer twitbot');
}

/**
 * Twitbot action callback for fetching a drupal user account by twitter
 * username.
 * 
 * @param string $twitter_username
 *   The twitter username connected to the account.
 * 
 * @throws RulesEvaluationException 
 * 
 * @return array
 */
function twitbot_rules_action_user_fetch($twitter_uid) {
  module_load_include('inc', 'twitbot', 'twitbot');
  $return = twitbot_user_load_by_twitter_uid($twitter_uid);
  $account = reset($return);
  if (!$account) {
    throw new RulesEvaluationException('Unable to load user by Twitter username "@username"', array('@username' => $twitter_username));
  }
  return array('user' => $account);
}
