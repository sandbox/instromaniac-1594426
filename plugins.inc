<?php
/*
 * @file plugins.inc
 * 
 * Helper functions for using the CTools plugin types defined by TwitBot.
 * 
 * TwitBot defines the following types of CTools plugins:
 * - channels: channels are the meduim through which data is fetched. Examples
 *     include the Twitter REST API and the Twitter Streaming API.
 * - methods: method plugins define how data is fetched on a channel. Examples 
 *     include user mentions, hashtags and saved searches.
 */

/**
 * Fetch metadata on a specific TwitBot channel plugin.
 *
 * @param string $channel
 *   Name of a TwitBot fchannel.
 *
 * @return
 *   An array with information about the requested TwitBot channel.
 */
function twitbot_get_channel($channel) {
  ctools_include('plugins');
  return ctools_get_plugins('twitbot', 'channels', $channel);
}

/**
 * Fetch metadata for all TwitBot channel plugins.
 *
 * @return
 *   An array of arrays with information about all available TwitBot channels.
 */
function twitbot_get_channels() {
  ctools_include('plugins');
  return ctools_get_plugins('twitbot', 'channels');
}

/**
 * Fetch metadata on a specific TwitBot method plugin.
 *
 * @param string $method
 *   Name of a TwitBot fetching method.
 *
 * @return
 *   An array with information about the requested TwitBot method.
 */
function twitbot_get_method($method) {
  ctools_include('plugins');
  return ctools_get_plugins('twitbot', 'methods', $method);
}

/**
 * Fetch metadata for all TwitBot method plugins.
 *
 * @return
 *   An array of arrays with information about all available TwitBot methods.
 */
function twitbot_get_methods() {
  ctools_include('plugins');
  return ctools_get_plugins('twitbot', 'methods');
}
