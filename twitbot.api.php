<?php

/**
 * @file
 * Hooks provided by the TwitBot module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Act on an incoming Twitter mention.
 *
 * @param TwitterStatus $mention
 *   A TwitterStatus object containing all the mention data.
 * @param TwitterUser $twitter_account
 *   The Twitter account of the user that was mentioned.
 *
 * @see twitbot_mention_process()
 */
function hook_twitbot_mention_process($mention, $twitter_account) {
  
}

/**
 * @} End of "addtogroup hooks".
 */
