Monitors a twitter stream, launching a rules event whenever an incoming tweet
is received. Messages can be parsed into commands with parameters and twitter
user names can be linked to drupal user accounts.