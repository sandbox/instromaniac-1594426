<?php

/**
 * @todo abstract fetching methods into separate modules. Look into ctools
 * plugins for this. Plugin types: channel (rest api, stream api), method (tweet,
 * search, hashtag).
 * @todo build admin interface
 * @todo document ctools plugins
 * ------
 * @todo look into using entities for twitter module.
 * @todo add alter hooks, make plugable.
 * @todo look into twitter streaming api.
 * @todo look into a parser for the message (rules action).
 * @todo add support for searches
 * @todo add support for hashtags
 * @todo tests
 */

/**
 * Implements hook_cron_queue_info().
 * 
 * Define the queue workers for processing twitter mentions.
 */
function twitbot_cron_queue_info() {
  $queues = array();
  $queues['twitbot_mentions_fetch'] = array(
    'worker callback' => 'twitbot_worker_mentions_fetch',
    'time' => 180,
  );
  $queues['twitbot_mention_process'] = array(
    'worker callback' => 'twitbot_worker_mention_process',
    'time' => 20,
  );
  return $queues;
}

/**
 * Implements hook_cron().
 * 
 * Create a queue item for each user account to fetch mentions for and every
 * unprocessed mention.
 * Two DrupalQueue instances are set up:
 * - twitbot_mentions_fetch: fetches mentions for drupal accounts.
 * - twitbot_mention_process: processes new mentions.
 */
function twitbot_cron() {
  module_load_include('inc', 'twitbot');
  // Queue twitter accounts for fetching mentions.
  $result = db_query_range("
    SELECT * FROM {twitbot_account} 
    WHERE handle_mentions = :handle_mentions ORDER BY last_refresh ASC", 0, 20, array(':handle_mentions' => 1));
  $refresh = time();
  $queue = DrupalQueue::get('twitbot_mentions_fetch');
  foreach($result as $item) {
    // Try adding the account to the queue.
    if ($queue->createItem($item)) {
      // Set a new refresh timestamp for this twitter_uid.
      $item->last_refresh = $refresh;
      twitbot_twitter_account_meta_save($item);
    }
  }
  // Queue unprocessed mentions. We need a reliable queue because mentions need
  // to be processed in the right order.
  // The INNER JOIN is needed so we can handle purged twitter posts.
  $queue = DrupalQueue::get('twitbot_mention_process', TRUE);
  $result = db_query_range("
    SELECT tp.* FROM {twitbot_posts} tp 
    INNER JOIN {twitter} t ON t.twitter_id = tp.twitter_id
    WHERE tp.is_processed = :is_processed AND tp.is_queued = :is_queued 
    ORDER BY tp.twitter_id ASC", 0, 100, array(':is_processed' => 0, ':is_queued' => 0));
  foreach($result as $item) {
    // Try adding the account to the queue.
    if ($queue->createItem($item)) {
      // Set a new refresh timestamp for this twitter_uid.
      $item->is_queued = 1;
      twitbot_twitter_status_meta_save($item);
    }
  }
}

/**
 * Implements hook_permission().
 */
function twitbot_permission() {
  return array(
    'process twitbot mentions' => array(
      'title' => t('Allow user to opt-in to have twitter mentions processed.'),
    ),
    'administer twitbot' => array(
      'title' => t('Allow user to administer twitbot mentions and settings.'),
    ),
  );
}

/**
 * Implements hook_user_delete().
 * 
 * Deletes all twitbot meta data for the user account.
 */
function twitbot_user_delete($account) {
  if (isset($account->twitter_accounts)) {
    foreach ($account->twitter_accounts as $twitter_account) {
      // Remove the twitter account meta data.
      db_delete('twitbot_account')
        ->condition('twitter_uid', $twitter_account->id)
        ->execute();
      // Remove the mention meta data.
      db_delete('twitbot_posts')
        ->condition('target_twitter_uid', $twitter_account->id)
        ->execute();
    }
  }
}

/**
 * Implements hook_ctools_plugin_type() .
 * 
 * Informs CTools about method and source plugins.
 */
function twitbot_ctools_plugin_type() {
  $plugins = array();
  // Channel plugins allow you to define the channel through which mentions
  // are fetched. Examples include the Twitter REST API and the Twitter 
  // Stresming API.
  $plugins['channels'] = array();
  // Method plugins allow developers to define how mentions are fetched. For
  // example: account mentions, hashtag or searches.
  $plugins['methods'] = array();
  
  return $plugins;
}

/**
 * Implements hook_ctools_plugin_directory().
 * 
 * Tell CTools where TwitBot's own plugin implementations live. 
 */
function twitbot_ctools_plugin_directory($module, $plugin) {
  if ($module == 'twitbot') {
    // We implement all our own plugin types.
    return 'plugins/' . $plugin;
  }
}

/**
 * Worker Callback for the twitbot_fetch_mentions queue. The worker will fetch
 * and store all new mentions for a twitter user ID.
 * 
 * @param object $account
 *   The twitter account to process. Should have these properties:
 *   - twitter_uid: the twitter id of the user to fetch mentions for.
 *   - last_twitter_id: the twitter ID of the last mention fetched for this
 *     account. Defaults to 0.
 */
function twitbot_worker_mentions_fetch($account) {
  module_load_include('inc', 'twitbot');
  // Fetch new mentions.
  $mentions = twitbot_mentions_fetch($account->twitter_uid, $account->since_twitter_id);
  if (count($mentions) > 0) {
    // Update the account meta data with last message ID.
    $last_mention = array_pop($mentions);
    $account->since_twitter_id = $last_mention->id;
    twitbot_twitter_account_meta_save($account);
  }
}

/**
 * Worker Callback for the twitbot_process_mention queue. The worker will load
 * the mention from the persistent storage and process it (e.g. run rules events
 * on it).
 * 
 * @param object $mention
 *   An object loaded from the {twitbot_posts} table and a target_twitter_uid 
 *   property containing the target user twitter ID.
 */
function twitbot_worker_mention_process($mention) {
  module_load_include('inc', 'twitbot');
  $mention = twitbot_twitter_status_load($mention->twitter_id);
  $account = twitbot_twitter_account_load($mention->twitbot->target_twitter_uid);
  twitbot_mention_process($mention, $account);
}
